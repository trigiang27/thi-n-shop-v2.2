$(document).ready(function() {
    $('#dataTables-example').DataTable({
            responsive: true
    });
});

$("div.alert").delay(3000).slideUp();

function xacnhanxoa (msg){
    if (window.confirm(msg)){
        return true;
    }
    return false;
}
$(document).ready(function (){
    $("#addImages").click(function (){
        $("#insert").append('<lable>Hình ảnh chi tiết sản phẩm</lable><div class="image_detail"><input type="file" name="fProductDetail[]" onchange="required_title(this)"/></div>');
        if($(".btn-them").length == 0){
            $(".box-right").append('<button type="submit" class="btn btn-success btn-them">Thêm</button>');
        }
    });
});

$(document).ready(function (){
    $("a#close").on('click',function (){
        var a = $(this).parent().attr("class");
        var url = "admin/"+a+"/delimg/";
        var _token = $("form[name='frmEdit']").find("input[name='_token']").val();
        var idHinh = $(this).parent().parent().attr("id");
        $.ajax({
            url: url + idHinh,
            type: 'GET',
            cache: false,
            data: {"_token":_token,"idHinh":idHinh},
            success: function (data) {
                if (data == "Ok") {
                    $("#" + idHinh).remove();
                } else {
                    alert("Error ! Vui lòng liên hệ Admin"); 
                }
            }
        });
    });
    $(".update_title").on('click', function (){
        var idHinh = $(this).parent().parent().attr('id');
        var _token = $("form[name='frmEdit']").find("input[name='_token']").val();
        var url = "admin/banner/update/" + idHinh;
        var content = $(this).prev().val();
        $.ajax({
            url: url,
            type: 'POST',
            cache: false,
            data: {"_token":_token, "idHinh":idHinh, "content":content},
            success: function (data) {
                if(data == "OK"){
                    alert("Cập nhật thành công!");
                } else {
                    alert("Error ! Vui lòng liên hệ Admin")
                }
            }
        });
    });
});
