$( document ).ready(function() {
    // slick
    //new-product and sale-off-product
    $('.new-product, .sale-off-product').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows:false,
        autoplay: true,
        autoplaySpeed: 8000,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows:false,
                    autoplay: true,
                    autoplaySpeed: 8000,
                }
            }
        ]
    });
    // highlight-product
    $('.highlight-product').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows:false,
        autoplay: true,
        autoplaySpeed: 8000,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows:false,
                    autoplay: true,
                    autoplaySpeed: 8000,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows:false,
                    autoplay: true,
                    autoplaySpeed: 8000,
                }
            }
        ]
    });
    $('.buy-more-product').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        rows: 3,
        arrows:false,
        autoplay: true,
        autoplaySpeed: 8000,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    rows: 3,
                    arrows:false,
                    autoplay: true,
                    autoplaySpeed: 8000,
                }
            }
        ]
    });
    $('.extra-buy-more-product, .extra-new-product').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        rows: 3,
        arrows:false,
        autoplay: true,
        autoplaySpeed: 8000,
    });
    function xacnhanxoa (msg){
        if (window.confirm(msg)){
            return true;
        }
        return false;
    }
    if(window.innerWidth >= 992){
        $("#detail-image").elevateZoom({
            zoomType: "inner",
            cursor: "crosshair",
            easing: "true",
        });
    }
    $(".glyphicon-trash").click(function (){
        if(window.confirm("Bạn có thật sự muốn xóa sản phẩm này khỏi giỏ hàng?")){
            var id = $(this).attr("class").split(" ")[0];
            $.ajax({
                url: 'giohang/delete/'+id,
                type: 'GET',
                cache: false,
                success: function (data){
                    $("."+id).parent().parent().remove();
                    $(".total").html(data);
                }
            });
        }
    });
    $(".update-cart").click(function (){
        var _token = $(".table-cart input[name='_token']").val();
        var url = "giohang/update";
        var data = {"_token":_token};
        data["data"] = [];
        $(".table-cart .inp-number").each(function (){
            var qty = $(this).val();
            var id = $(this).parent().parent().find(".glyphicon-trash").attr("class").split(" ")[0];
            var value = [id, qty];
            data["data"].push(value);
        });
        $.ajax({
            url: url,
            type: 'POST',
            cache: false,
            data: data,
            success: function (response) {
                $(".price_qty").each(function (){
                    var id = $(this).parent().parent().find(".glyphicon-trash").attr("class").split(" ")[0];
                    $(this).html(response[0][id].price*response[0][id].qty);
                });
                $(".total").html(response[1]);
                var content = '<div id="alert" class="alert alert-success" style="display:none;"></div>';
                var notify = $(".btn-cart")[0];
                $(notify).prepend(content);
                var alert = $("#alert");
                alert.html("Cập nhật thành công");
                alert.slideDown();
                alert.delay(3000).slideUp(function (){
                    alert.remove();
                });
            }
        });
    });
    // navbar active
    path = window.location.pathname;
    switch(path){
        case "/":
            $($(".trang-chu")[0]).addClass("actived");
            break;
        case "/gioi-thieu":
            $($(".gioi-thieu")[0]).addClass("actived");
            break;
        case "/thong-tin":
            $($(".thong-tin")[0]).addClass("actived");
            break;
        case "/lien-he":
            $($(".lien-he")[0]).addClass("actived");
            break;
        case "/cham-soc":
            $($(".cham-soc")[0]).addClass("actived");
            break;
    }
    if(path.indexOf('san-pham') !== -1){
        $($(".san-pham")[0]).addClass("actived");
    }
    $($(".carousel-inner").children()[0]).addClass("active");
    $($(".carousel-indicators").children()[0]).addClass("active");
    
    // setTimeout(function(){
    //     var height = [];
    //     $(".carousel-inner").children().each(function (){
    //         height.push($(this).height());
    //     });
    //     var max = Math.max.apply(Math, height);
    //     $(".carousel-inner").children().each(function (){
    //         $(this).css('height', max);
    //     });
    // }, 1000);
    
});