<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'saleoff',
        'description',
        'content',
        'image',
        'highlight',
        'salling',
        'status',
        'cate_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function cate(){
        return $this->belongsTo('App\Cate');
    }
    
    public function product_images(){
        return $this->hasMany('App\ProductImage');
    }
}
