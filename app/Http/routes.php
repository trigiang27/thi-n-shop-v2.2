<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('', 'PagesController@index');
Route::get('gioi-thieu', 'PagesController@getGioithieu');
Route::get('san-pham/{alias}', 'PagesController@cate');
Route::get('san-pham-moi', 'PagesController@newProduct');
Route::get('san-pham-khuyen-mai', 'PagesController@saleoffProduct');
Route::get('san-pham-noi-bat', 'PagesController@highlightProduct');
Route::get('san-pham-mua-nhieu', 'PagesController@sallingProduct');
Route::get('chi-tiet/{pro_alias}', 'PagesController@detail');
Route::get('{pro_alias}/san-pham-lien-quan', 'PagesController@concernProduct');
Route::get('lien-he', 'PagesController@getLienhe');
Route::post('lien-he', 'PagesController@postLienhe');
Route::get('thong-tin', 'PagesController@getThongtin');
Route::get('dat-hang', 'PagesController@getDathang');
Route::post('dat-hang', 'PagesController@postDathang');
Route::get('tim-kiem', 'PagesController@getTimkiem');
Route::get('cham-soc', 'PagesController@getChamSoc');

//Gio hang
Route::group(['prefix' => 'giohang'], function(){
    Route::get('', 'PagesController@CartShow');
    Route::post('add/{id}', 'PagesController@CartAdd');
    Route::get('delete/{id}', 'PagesController@CartDelete');
    Route::post('update', 'PagesController@CartUpdate');
});

// Admin
Route::get('admin/login',[
    'as' => 'admin.getLogin',
    'uses' => 'UserController@getLoginAdmin'
]);
Route::post('admin/login',[
    'as' => 'admin.postLogin',
    'uses' => 'UserController@postLoginAdmin'
]);
Route::get('admin/logout',[
    'as' => 'admin.logout',
    'uses' => 'UserController@getLogout'
]);
Route::group(['prefix' => 'admin', 'middleware' => 'adminlogin'],function(){
    Route::group(['prefix' => 'cate'],function(){
        Route::get('list',[
            'as' => 'admin.cate.list',
            'uses' => 'CateController@getList'
        ]);
        Route::get('create',[
            'as' => 'admin.cate.getCreate',
            'uses' => 'CateController@getCreate'
        ]);
        Route::post('create',[
            'as' => 'admin.cate.postCreate',
            'uses' => 'CateController@postCreate'
        ]);
        Route::get('edit/{id}',[
            'as' => 'admin.cate.getEdit',
            'uses' => 'CateController@getEdit'
        ]);
        Route::post('edit/{id}',[
            'as' => 'admin.cate.postEdit',
            'uses' => 'CateController@postEdit'
        ]);
        Route::get('delete/{id}',[
            'as' => 'admin.cate.delete',
            'uses' => 'CateController@delete'
        ]);
    });
    Route::group(['prefix' => 'product'],function (){
        Route::get('list',[
            'as' => 'admin.product.list',
            'uses' => 'ProductController@getList'
        ]);
        Route::get('create',[
            'as' => 'admin.product.getCreate',
            'uses' => 'ProductController@getCreate'
        ]);
        Route::post('create',[
            'as' => 'admin.product.postCreate',
            'uses' => 'ProductController@postCreate'
        ]);
        Route::get('edit/{id}',[
            'as' => 'admin.product.getEdit',
            'uses' => 'ProductController@getEdit'
        ]);
        Route::post('edit/{id}',[
            'as' => 'admin.product.postEdit',
            'uses' => 'ProductController@postEdit'
        ]);
        Route::get('delete/{id}',[
            'as' => 'admin.product.delete',
            'uses' => 'ProductController@delete'
        ]);
        Route::get('delimg/{id}',[
            'as' => 'admin.product.getDelImg',
            'uses' => 'ProductController@getDelImg'
        ]);
        Route::get('update-property/{id}/{property}',[
            'as' => 'admin.product.update-property',
            'uses' => 'ProductController@update_property'
        ]);
    });
    Route::group(['prefix' => 'user'],function(){
        Route::get('list',[
            'as' => 'admin.user.list',
            'uses' => 'UserController@getList'
        ]);
        Route::get('create',[
            'as' => 'admin.user.getCreate',
            'uses' => 'UserController@getCreate'
        ]);
        Route::post('create',[
            'as' => 'admin.user.postCreate',
            'uses' => 'UserController@postCreate'
        ]);
        Route::get('edit/{id}',[
            'as' => 'admin.user.getEdit',
            'uses' => 'UserController@getEdit'
        ]);
        Route::post('edit/{id}',[
            'as' => 'admin.user.postEdit',
            'uses' => 'UserController@postEdit'
        ]);
        Route::get('delete/{id}',[
            'as' => 'admin.user.delete',
            'uses' => 'UserController@delete'
        ]);
    });
    Route::group(['prefix' => 'banner'], function (){
        Route::get('list',[
            'as' => 'admin.banner.getList',
            'uses' => 'BannerController@getList'
        ]);
        Route::post('create',[
            'as' => 'admin.banner.postCreate',
            'uses' => 'BannerController@postCreate'
        ]);
        Route::post('update/{id}',[
            'as' => 'admin.banner.postUpdate',
            'uses' => 'BannerController@postUpdate'
        ]);
        Route::get('delimg/{id}',[
            'as' => 'admin.banner.getDelImg',
            'uses' => 'BannerController@getDelImg'
        ]);
    });
    Route::group(['prefix' => 'noi-dung'], function (){
        Route::get('gioi-thieu',[
            'as' => 'admin.noidung.getGioithieu',
            'uses' => 'ContentController@getGioiThieu'
        ]);
        Route::post('gioi-thieu',[
            'as' => 'admin.noidung.postGioithieu',
            'uses' => 'ContentController@postGioiThieu'
        ]);
        Route::get('thong-tin',[
            'as' => 'admin.noidung.getThongtin',
            'uses' => 'ContentController@getThongTin'
        ]);
        Route::post('thong-tin',[
            'as' => 'admin.noidung.postThongtin',
            'uses' => 'ContentController@postThongTin'
        ]);
        Route::get('cham-soc',[
            'as' => 'admin.noidung.getChamsoc',
            'uses' => 'ContentController@getChamSoc'
        ]);
        Route::post('cham-soc',[
            'as' => 'admin.noidung.postChamsoc',
            'uses' => 'ContentController@postChamSoc'
        ]);
    });
});
