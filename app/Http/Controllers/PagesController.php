<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Cate;
use App\Product;
use App\Manager;
use App\Banner;
use Mail,Cart;

class PagesController extends Controller
{
    public function __construct(){
        $cates = Cate::select('id','name','alias')->get();
        $salling_product = Product::where('salling','=',1)->orderBy('id','DESC')->take(12)->get();
        $count_cart = Cart::content()->count();
        $new_product = Product::select('id','name','alias','price','saleoff','image')->where('new','=',1)->orderBy('id','DESC')->take(12)->get();
        $banners = Banner::select('id', 'name', 'title')->orderBy('id','DESC')->get();
        view()->share('cates',$cates);
        view()->share('salling_product',$salling_product);
        view()->share('count_cart',$count_cart);
        view()->share('new_product',$new_product);
        view()->share('banners',$banners);
    }

    public function index(){
        $saleoff_product = Product::select('id','name','alias','price','saleoff','image')->orderBy('saleoff','DESC')->take(8)->get();
        $highlight_product = Product::select('id','name','alias','price','saleoff','image')->where('highlight','=',1)->orderBy('id','DESC')->take(8)->get();
        return view('pages.home', compact('new_product','saleoff_product','highlight_product'));
    }

    public function cate($alias){
        $cate = Cate::where('alias',$alias)->first();
        $products = Product::where('cate_id',$cate->id)->orderBy('id','DESC')->paginate(12);
        return view('pages.cate',compact('cate','products'));
    }

    public function pagesProduct($page)
    {
        switch ($page) {
            case "moi" :
                $products = Product::select('id','name','alias','price','saleoff','image')->orderBy('id','DESC')->paginate(12);
                $name = "SẢN PHẨM MỚI";
                break;
            case "khuyen-mai" :
                $products = Product::select('id','name','alias','price','saleoff','image')->where('saleoff',">",0)->orderBy('saleoff','DESC')->paginate(12);
                $name = "SẢN KHUYẾN MÃI";
                break;
            case "noi-bat" :
                $products = Product::select('id','name','alias','price','saleoff','image')->where('highlight',"=",1)->orderBy('saleoff','DESC')->paginate(12);
                $name = "SẢN NỔI BẬT";
                break;
            case "mua-nhieu" :
                $products = Product::select('id','name','alias','price','saleoff','image')->where('salling',"=",1)->orderBy('saleoff','DESC')->paginate(12);
                $name = "SẢN MUA NHIỀU";
                break;
            default:
                return redirect('');
        }
        return view('pages.pagesProduct', compact('products','name'));
    }

    public function newProduct(){
        $products = Product::select('id','name','alias','price','saleoff','image')->where('new', '=', 1)->orderBy('id','DESC')->paginate(12);
        $name = "SẢN PHẨM MỚI";
        return view('pages.pagesProduct', compact('products', 'name'));
    }

    public function saleoffProduct(){
        $products = Product::select('id','name','alias','price','saleoff','image')->where('saleoff',">",0)->orderBy('saleoff','DESC')->paginate(12);
        $name = "SẢN KHUYẾN MÃI";
        return view('pages.pagesProduct', compact('products', 'name'));
    }

    public function highlightProduct(){
        $products = Product::select('id','name','alias','price','saleoff','image')->where('highlight',"=",1)->orderBy('saleoff','DESC')->paginate(12);
        $name = "SẢN NỔI BẬT";
        return view('pages.pagesProduct', compact('products', 'name'));
    }

    public function sallingProduct(){
        $products = Product::select('id','name','alias','price','saleoff','image')->where('salling',"=",1)->orderBy('saleoff','DESC')->paginate(12);
        $name = "SẢN MUA NHIỀU";
        return view('pages.pagesProduct', compact('products', 'name'));
    }

    public function detail($pro_alias)
    {
        $product = Product::where('alias', $pro_alias)->first();
        $cate_id = $product->cate->id;
        $concern_product = Product::select('id','name','alias','price','saleoff','image','description')->where('cate_id',$cate_id)->orderBy('id','DESC')->take(8)->get();
        return view('pages.detail', compact('product','concern_product'));
    }

    public function concernProduct($pro_alias)
    {
        $product = Product::where('alias', $pro_alias)->first();
        $cate_id = $product->cate->id;
        $concern_product = Product::select('id','name','alias','price','saleoff','image')->where('cate_id',$cate_id)->orderBy('id','DESC')->paginate(12);
        return view('pages.concernProduct', compact('concern_product'));
    }

    public function getLienhe()
    {
        return view('pages.lienhe');
    }

    public function postLienhe(Request $request)
    {
        $this->validate($request,
           [
                'name' => 'required',
                'sdt' => 'required',
                'message' => 'required|min:5|max:1000'
           ],
           [
                'name.required' => 'Vui lòng nhập tên của bạn.',
                'sdt.required' => 'Vui lòng nhập số điện thoại để chúng tôi có thể liên hệ với bạn.',
                'message.required' => 'Vui lòng nội dung tin nhắn.',
                'message.min' => 'Nội dung tin nhắn quá ngắn.',
                'message.max' => 'Nội dung tin nhắn không được quá 1000 ký tự.'
           ]
        );
        $data = ['name'=>$request->name,'sdt'=>$request->sdt,'messages'=>$request->message];
        Mail::send('emails.lienhe',$data,function ($msg) {
            $msg->from('nguyentrigiang19911@gmail.com','Customer');
            $msg->to('nguyentrigiang19911@gmail.com','Admin')->subject('Email Lien He');
        });
        echo "<script>
            alert('Cảm ơn bạn đã liên hệ với chúng tôi. Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất.');
            window.location = '".url('')."'
        </script>";
    }

    public function getThongtin()
    {
        $data = Manager::select('thongtin')->where('id', '=', 1)->first();
        return view('pages.thongtin', compact('data'));
    }

    public function getGioithieu()
    {
        $data = Manager::select('gioithieu')->where('id', '=', 1)->first();
        return view('pages.gioithieu', compact('data'));
    }

    public function getChamSoc()
    {
        $data = Manager::select('chamsoc')->where('id', '=', 1)->first();
        return view('pages.chamsoc', compact('data'));
    }

    public function CartShow()
    {
        $data = Cart::content();
        $total = Cart::subtotal();
        return view('pages.giohang',compact('data','total'));
    }

    public function CartAdd(Request $request, $id)
    {
        $product = Product::select('id','name','price', 'saleoff','image','alias', 'status')->where('id',$id)->first();
        $product->price = $product->price*(100-$product->saleoff)/100;
        Cart::add(array('id'=>$id,'name'=>$product->name,'qty'=>$request->quantity,'price'=>$product->price,'options'=>array('img'=>$product->image,'alias'=>$product->alias, 'status'=>$product->status)));
        return redirect('giohang');
    }

    public function CartUpdate(Request $request)
    {
        if($request->ajax()){
            foreach($request->data as $cart)
            {
                Cart::update($cart[0], $cart[1]);
            }
            $cart_content = Cart::content();
            $total = Cart::subtotal();
            return [$cart_content,$total];
        }
    }

    public function CartDelete(Request $request)
    {
        Cart::remove($request->id);
        $total = Cart::subtotal();
        return $total;
    }

    public function getDathang()
    {
        $data = Cart::content();
        $total = Cart::subtotal();
        return view('pages.dathang',compact('data','total'));
    }

    public function postDathang(Request $request)
    {
        $this->validate($request,
            [
                'txtName' => 'required|max:50',
                'txtPhone' => 'required|regex:/^[0-9]{10,11}$/',
                'txtAddress' => 'required',
                'txtNote' => 'max:2000'
            ],
            [
                'txtName.required' => 'Vui lòng nhập họ và tên.',
                'txtName.max' => 'Tên quá dài',
                'txtPhone.required' => 'Vui lòng nhập số điện thoại để chúng tôi có thể liên hệ với bạn.',
                'txtPhone.regex' => 'Số điện thoại không đúng',
                'txtAddress.required' => 'Vui lòng nhập địa chỉ giao hàng',
                'txtNote.max' => 'Ghi chú quá dài'
            ]
        );
        $tongtien = Cart::total();
        $data = ['name'=>$request->txtName, 'sdt'=>$request->txtPhone, 'diachi'=>$request->txtAddress, 'ghichu'=>$request->txtNote, 'tongtien'=>$tongtien];
        $data['giohang'] = Cart::content();
        Mail::send('emails.dathang',$data,function ($msg) {
            $msg->from('nguyentrigiang19911@gmail.com','Customer');
            $msg->to('nguyentrigiang19911@gmail.com','Admin')->subject('Đặt hàng');
        });
        echo "<script>
            alert('Cảm ơn bạn đã đặt hàng từ shop của chúng tôi! Chúng tôi sẽ liên hệ để xác nhận đơn hàng và giao hàng trong thời gian sớm nhất.');
            window.location = '".url('')."'
        </script>";
    }

    public function getTimkiem(Request $request)
    {
        $tukhoa = $request->tukhoa;
        $products = Product::select('id','name','alias','price','saleoff','image')->where('name','like',"%$tukhoa%")->orderBy('id','DESC')->paginate(12);;
        return view('pages.timkiem', compact('tukhoa','products'));
    }
}
