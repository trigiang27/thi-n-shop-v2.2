<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\File;

use App\Banner;

class BannerController extends Controller
{
    public function getList()
    {
        $data = Banner::select('id','name', 'title')->orderBy('id','DESC')->get();
        return view('admin.banner.list', compact('data'));
    }

    public function postCreate(Request $request)
    {
        $this->validate($request,
           [
                'fBanner' => 'required|image',
                'title_banner' => 'required'
           ],
           [
                'fBanner.required' => 'Vui lòng chọn hình ảnh.',
                'fBanner.image' => 'Đây không phải là ảnh. Vui lòng chọn đúng định dạng ảnh.',
                'title_banner.required' => 'Vui lòng nhập title cho hình ảnh.'
           ]
        );
        $banner = new Banner;
        $banner->title = $request->title_banner;
        //save image
        $file = $request->file('fBanner');
        $filename = $file->getClientOriginalName();
        $xxxx_filename = str_random(4)."_".$filename;
        while(file_exists('upload/banners/'.$xxxx_filename)) {
            $xxxx_filename = str_random(4)."_".$filename;
        }
        $banner->name = $xxxx_filename;
        $file->move('upload/banners/',$xxxx_filename);
        $banner->save();
        return redirect('admin/banner/list')->with(['flash_level' => 'success', 'flash_message' => 'Tạo thành công']);
    }

    public function postUpdate(Request $request)
    {
        if($request->ajax()){
            $banner = Banner::find($request->idHinh);
            $banner->title = $request->content;
            $banner->save();
            return "OK";
        }
    }

    public function getDelImg(Request $request)
    {
        if($request->ajax()){
            $idHinh = $request->idHinh; 
            $banner = Banner::find($idHinh);
            if (!empty($banner)) {
                $img = 'upload/banners/'.$banner->name;
                if (File::exists($img)) {
                    File::delete($img);
                }
                $banner->delete();
            }
            return "Ok"; 
        }
    }
}
