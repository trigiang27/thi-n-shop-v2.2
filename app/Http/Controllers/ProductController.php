<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Product;
use App\Cate;
use App\ProductImage;


class ProductController extends Controller
{
    public function getList()
    {
        $data = Product::select('id','name','price','saleoff','description','image','cate_id', 'new','highlight','salling','status')->orderBy('id','DESC')->get();
        return view('admin.product.list',compact("data"));
    }
    public function getCreate()
    {
        $cate = Cate::select('id','name','parent_id')->get();
        return view('admin.product.create',compact("cate"));
    }
    public function postCreate(Request $request)
    {
        $this->validate($request,
            [
                'sltParent' => 'required',
                'txtName' => 'required|max:50|unique:products,name',
                'txtPrice' => 'required|integer|min:1000',
                'txtSaleOff' => 'integer|min:0|max:99',
                'fImages' => 'required|image',
                'txtDescription' => 'required|max:300'
            ],
            [
                'sltParent.required' => 'Vui lòng chọn thể loại.',
                'txtName.required' => 'Vui lòng nhập tên sản phẩm.',
                'txtName.max' => 'Tên sản phẩm không được quá 50 ký tự.',
                'txtName.unique' => 'Tên sản phẩm đã tồn tại.',
                'txtPrice.required' => 'Vui lòng nhập giá sản phẩm.',
                'txtPrice.integer' => 'Giá phải là số nguyên dương, không được có . hoặc ,',
                'txtPrice.min' => 'Giá không được thấp hơn 1000',
                'txtSaleOff.integer' => 'Khuyến mãi phải là số nguyên từ 0 đến 99 (Vd: 10 là 10%)',
                'txtSaleOff.min' => 'Khuyến mãi phải là số nguyên từ 0 đến 99 (Vd: 10 là 10%)',
                'txtSaleOff.max' => 'Khuyến mãi phải là số nguyên từ 0 đến 99 (Vd: 10 là 10%)',
                'fImages.required' => 'Vui lòng chọn ảnh sản phẩm.',
                'fImages.image' => 'Đây không phải là ảnh. Vui lòng chọn đúng định dạng ảnh.',
                'txtDescription.required' => 'Vui lòng nhập mô tả sản phẩm.',
                'txtDescription.max' => 'Mô tả không được dài hơn 300 ký tự.'
            ]
        );
        $product = new Product;
        $product->cate_id = $request->sltParent;
        $product->name = $request->txtName;
        $product->alias = changeTitle($request->txtName);
        $product->price = $request->txtPrice;
        $product->saleoff = $request->txtSaleOff;
        $product->description = $request->txtDescription;
        $product->content = $request->txtContent;
        $product->new = $request->rdoNew;
        $product->highlight = $request->rdoHighLile;
        $product->salling = $request->rdoSalling;
        $product->status = $request->rdoStatus;
        //save image
        $file = $request->file('fImages');
        $filename = $file->getClientOriginalName();
        $xxxx_filename = str_random(4)."_".$filename;
        while(file_exists('upload/'.$xxxx_filename)) {
            $xxxx_filename = str_random(4)."_".$filename;
        }
        $product->image = $xxxx_filename;
        $file->move('upload/',$xxxx_filename);
        $product->save();
        //image_detail
        // $product_id = $product->id;
        // $file_detail = $request->file('fProductDetail');
        // if(isset($file_detail)) {
        //     foreach($file_detail as $file) {
        //         if(isset($file)) {
        //             $product_img = new ProductImage();
        //             $filename = $file->getClientOriginalName();
        //             $xxxx_filename = str_random(4)."_".$filename;
        //             while(file_exists('upload/detail/'.$xxxx_filename)) {
        //                 $xxxx_filename = str_random(4)."_".$filename;
        //             }
        //             $product_img->name = $xxxx_filename;
        //             $product_img->product_id = $product_id;
        //             $file->move('upload/detail/',$xxxx_filename);
        //             $product_img->save();
        //         }
        //     }
        // }
        return redirect('admin/product/create')->with(['flash_level' => 'success', 'flash_message' => 'Tạo thành công']);
    }
    public function getEdit($id)
    {
        $cate = Cate::select('id','name','parent_id')->get();
        $data = Product::findOrFail($id);
        return view('admin.product.edit',compact('cate','data'));
    }
    public function postEdit(Request $request, $id)
    {
        $this->validate($request,
            [
                'sltParent' => 'required',
                'txtName' => 'required|max:50|unique:products,name,'.$id,
                'txtPrice' => 'required|integer|min:1000',
                'txtSaleOff' => 'integer|min:0|max:99',
                'fImages' => 'image',
                'txtDescription' => 'max:300'
            ],
            [
                'sltParent.required' => 'Vui lòng chọn thể loại.',
                'txtName.required' => 'Vui lòng nhập tên sản phẩm.',
                'txtName.max' => 'Tên sản phẩm không được quá 50 ký tự.',
                'txtName.unique' => 'Tên sản phẩm đã tồn tại.',
                'txtPrice.required' => 'Vui lòng nhập giá sản phẩm.',
                'txtPrice.integer' => 'Giá phải là số nguyên dương, không được có . hoặc ,',
                'txtPrice.min' => 'Giá không được thấp hơn 1000',
                'txtSaleOff.integer' => 'Khuyến mãi phải là số nguyên từ 0 đến 99 (Vd: 10 là 10%)',
                'txtSaleOff.min' => 'Khuyến mãi phải là số nguyên từ 0 đến 99 (Vd: 10 là 10%)',
                'txtSaleOff.max' => 'Khuyến mãi phải là số nguyên từ 0 đến 99 (Vd: 10 là 10%)',
                'fImages.image' => 'Đây không phải là ảnh. Vui lòng chọn đúng định dạng ảnh.',
                'txtDescription.max' => 'Mô tả không được dài hơn 300 ký tự.'
            ]
        );
        //Edit information
        $product = Product::findOrFail($id);
        $product->cate_id = $request->sltParent;
        $product->name = $request->txtName;
        $product->alias = changeTitle($request->txtName);
        $product->price = $request->txtPrice;
        $product->saleoff = $request->txtSaleOff;
        $product->description = $request->txtDescription;
        $product->content = $request->txtContent;
        $product->new = $request->rdoNew;
        $product->highlight = $request->rdoHighLile;
        $product->salling = $request->rdoSalling;
        $product->status = $request->rdoStatus;
        //Edit image
        $file = $request->file('fImages');
        if(!empty($file)){
            //Delete current image
            $img_current = "upload/".$product->image;
            if(File::exists($img_current)){
                File::delete($img_current);
            }
            //get new image
            $filename = $file->getClientOriginalName();
            $xxxx_filename = str_random(4)."_".$filename;
            while(file_exists('upload/'.$xxxx_filename)) {
                $xxxx_filename = str_random(4)."_".$filename;
            }
            $product->image = $xxxx_filename;
            $file->move('upload/',$xxxx_filename);
        }
        $product->save();
        //Edit image-detail
        // $file_detail = $request->file('fProductDetail');
        // if(isset($file_detail)) {
        //     foreach($file_detail as $file) {
        //         if(isset($file)) {
        //             $product_img = new ProductImage();
        //             $filename = $file->getClientOriginalName();
        //             $xxxx_filename = str_random(4)."_".$filename;
        //             while(file_exists('upload/detail/'.$xxxx_filename)) {
        //                 $xxxx_filename = str_random(4)."_".$filename;
        //             }
        //             $product_img->name = $xxxx_filename;
        //             $product_img->product_id = $id;
        //             $file->move('upload/detail/',$xxxx_filename);
        //             $product_img->save();
        //         }
        //     }
        // }
        return redirect("admin/product/list")->with(['flash_level'=> 'success','flash_message'=>'Sửa thành công']);
    }
    public function delete($id)
    {
        $product = Product::findOrFail($id);
        File::delete('upload/'.$product->image);
        $product_detail = $product->product_images;
        foreach($product_detail as $detail){
            File::delete('upload/detail/'.$detail->name);
        }
        $product->delete();
        return redirect('admin/product/list')->with(['flash_level' => 'success', 'flash_message' => 'Xóa thành công']);
    }
    public function getDelImg (Request $request, $id)
    {
        if ($request->ajax()) {
            $idHinh = $request->idHinh; 
            $image_detail = ProductImage::find($idHinh);
            if (!empty($image_detail)) {
                $img = 'upload/detail/'.$image_detail->name;
                if (File::exists($img)) {
                    File::delete($img);
                }
                $image_detail->delete();
            }
            return "Ok"; 
        }
    }

    public function update_property (Request $request)
    {
        if($request->ajax()){
            $id = $request->id;
            $property = $request->property;
            $product = Product::findOrFail($id);
            $val_property = $product->$property;
            if($val_property === 1){
                $product->$property = 0;
                $product->save();
                echo 0;
            } else {
                $product->$property = 1;
                $product->save();
                echo 1;
            }
        }
    }
}

