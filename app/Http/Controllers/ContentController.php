<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Manager;

class ContentController extends Controller
{

    public function __construct()
    {
        $content = Manager::select('gioithieu', 'thongtin', 'chamsoc')->get();
        view()->share('content',$content);
    }

    public function getGioiThieu()
    {
        return view('admin.managers.intro');
    }

    public function postGioiThieu(Request $request)
    {
        $data = Manager::findOrFail(1);
        $data->gioithieu = $request->txtContent;
        $data->save();
        return redirect('admin/noi-dung/gioi-thieu')->with(['flash_level' => 'success', 'flash_message' => 'Sửa thành công']);
    }

    public function getThongTin()
    {
        return view('admin.managers.contact');
    }

    public function postThongTin(Request $request)
    {
        $data = Manager::findOrFail(1);
        $data->thongtin = $request->txtContent;
        $data->save();
        return redirect('admin/noi-dung/thong-tin')->with(['flash_level' => 'success', 'flash_message' => 'Sửa thành công']);
    }

    public function getChamSoc()
    {
        return view('admin.managers.chamsoc');
    }

    public function postChamSoc(Request $request)
    {
        $data = Manager::findOrFail(1);
        $data->chamsoc = $request->txtContent;
        $data->save();
        return redirect('admin/noi-dung/cham-soc')->with(['flash_level' => 'success', 'flash_message' => 'Sửa thành công']);
    }
}
