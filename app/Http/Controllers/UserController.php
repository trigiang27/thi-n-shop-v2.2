<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Hash;
use Auth;

class UserController extends Controller
{
    public function getList()
    {
        $data = User::select('id','username','level')->orderBy('id','DESC')->get();
        return view('admin.user.list',compact('data'));
    }
    public function getCreate()
    {
        return view('admin.user.create');
    }
    public function postCreate(Request $request)
    {
        $this->validate($request,
            [
                'txtUser' => 'required|unique:users,username|min:6|max:30',
                'txtPass' => 'required|min:8|max:50',
                'txtRePass' => 'required|same:txtPass',
            ],
            [
                'txtUser.required' => 'Vui lòng nhập tên tài khoản.',
                'txtUser.unique' => 'Tên tài khoản đã tồn tại.',
                'txtUser.min' => 'Tên tài khoản phải từ 6 đến 30 ký tự.',
                'txtUser.max' => 'Tên tài khoản phải từ 6 đến 30 ký tự.',
                'txtPass.required' => 'Vui lòng nhập mật khẩu.',
                'txtPass.min' => 'Mật khẩu phải có ít nhất 8 ký tự.',
                'txtPass.max' => 'Mật khẩu phải ít hơn 50 ký tự.',
                'txtRePass.required' => 'Vui lòng xác nhận mật khẩu.',
                'txtRePass.same' => 'Xác nhận mật khẩu không đúng.'
            ]
        );
        $user = New User;
        $user->username = $request->txtUser;
        $user->password = Hash::make($request->txtPass);
        $user->level = $request->rdoLevel;
        $user->remember_token = $request->_token;
        $user->save();
        return redirect('admin/user/create')->with(['flash_level' => 'success', 'flash_message' => 'Tạo thành công']);
    }
    public function getEdit($id)
    {
        $data = User::findOrFail($id);
        if ( (Auth::user()->id != 1) && ($id == 1 || ($data->level) == 1 && (Auth::user()->id != $id)) ) {
            return redirect('admin/user/list')->with(['flash_level'=>'danger','flash_message'=>'Bạn không thể sửa tài khoản này.']);
        }
        return view('admin.user.edit',compact('data'));
    }
    public function postEdit(Request $request,$id)
    {
        $user = User::findOrFail($id);
        if($request->input('txtPass') || $request->input('txtRePass')){
            $this->validate($request,[
                'txtPass' => 'required|min:8|max:50',
                'txtRePass' => 'required|same:txtPass',
            ],
            [
                'txtPass.required' => 'Vui lòng nhập mật khẩu.',
                'txtPass.min' => 'Mật khẩu phải có ít nhất 8 ký tự.',
                'txtPass.max' => 'Mật khẩu phải ít hơn 50 ký tự.',
                'txtRePass.required' => 'Vui lòng xác nhận mật khẩu.',
                'txtRePass.same' => 'Xác nhận mật khẩu không đúng.'
            ]);
            $user->password = Hash::make($request->txtPass);
        }
        $user->level = $request->rdoLevel;
        $user->remember_token = $request->_token;
        $user->save();
        return redirect('admin/user/list')->with(['flash_level' => 'success', 'flash_message' => 'Sửa thành công']);
    }
    public function delete($id)
    {
        $id_login_current = Auth::user()->id;
        $user_del = User::findOrFail($id);
        if($user_del->id == 1||($id_login_current != 1 && $user_del->level == 1)){
            return redirect('admin/user/list')->with(['flash_level' => 'danger', 'flash_message' => 'Bạn không thể xóa tài khoản này']);
        } else{
            $user_del->delete();
            return redirect('admin/user/list')->with(['flash_level' => 'success', 'flash_message' => 'Xóa thành công']);
        }
    }
    public function getLoginAdmin()
    { 
        if(Auth::check() && Auth::user()->level == 1){
            return redirect('admin/product/list');
        }
        return view('admin.login.login');
    }
    public function postLoginAdmin(Request $request)
    {
        $this->validate($request,
            [
                'textUser' => 'required',
                'txtPassword' => 'required'
            ],
            [
                'textUser.required' => 'Vui lòng nhập tài khoản,',
                'txtPassword.required' => 'Vui lòng nhập mật khẩu.'
            ]
        );
        $login = array(
            'username' => $request->textUser,
            'password' => $request->txtPassword,
            'level' => 1
        );
        if(Auth::attempt($login)){
            return redirect('admin/product/list');
        } else {
            return redirect()->back()->with(['flash_level'=>'danger','flash_message'=>'Tài khoản hoặc password không đúng.']);
        }
    }
    public function getLogout()
    {
        if(Auth::check()){
            Auth::logout();
            return redirect('admin/login');
        }
    }
}
