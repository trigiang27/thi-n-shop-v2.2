<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Cate;

class CateController extends Controller
{
     public function getList()
     {
          $data = Cate::select('id','name','parent_id')->orderBy('id','DESC')->get();
          return view('admin.cate.list',compact('data'));
     }
     public function getCreate(){
          $parent = Cate::select('id','name','parent_id')->get();
          return view('admin.cate.create',compact('parent'));
     }
     public function postCreate(Request $request)
     {
          $this->validate($request,
               [
                    'txtCateName' => 'required|unique:cates,name|max:20'
               ],
               [
                    'txtCateName.required' => 'Vui lòng nhập tên thể loại.',
                    'txtCateName.unique' => 'Tên loại sản phẩm đã tồn tại.',
                    'txtCateName.max' => 'Tên không được quá 20 ký tự.'
               ]
          );
          $cate = new Cate;
          $cate->name = $request->txtCateName;
          $cate->alias = changeTitle($request->txtCateName);
          $cate->parent_id = $request->sltParent;
          $cate->save();
          return redirect('admin/cate/create')->with(['flash_level' => 'success', 'flash_message' => 'Tạo thành công']);
     }
     public function getEdit($id)
     {
          $data = Cate::findOrFail($id);
          $parent = Cate::select('id','name','parent_id')->get();
          return view('admin.cate.edit',compact('parent','data'));
     }
     public function postEdit(Request $request,$id)
     {
          $this->validate($request,
               [
                    'txtCateName' => 'required|unique:cates,name|max:20,'.$id
               ],
               [
                    'txtCateName.required' => 'Vui lòng nhập tên thể loại.',
                    'txtCateName.unique' => 'Tên loại sản phẩm đã tồn tại.',
                    'txtCateName.max' => 'Tên không được quá 20 ký tự.'
               ]
          );
          $cate = Cate::find($id);
          $cate->name = $request->txtCateName;
          $cate->parent_id = $request->sltParent;
          $cate->save();
          return redirect('admin/cate/list')->with(['flash_level' => 'success', 'flash_message' => 'Sửa thành công']);
     }
     public function delete($id)
     {
          $parent = Cate::where('parent_id',$id)->count();
          if($parent == 0){
               $cate = Cate::find($id);
               $cate->delete();
               return redirect('admin/cate/list')->with(['flash_level' => 'success', 'flash_message' => 'Xóa thành công']);
          } else {
               echo "<script type='text/javascript'>
                    alert('Bạn không thể xóa thể loại này!');
                    window.location = '";
                         echo route('admin.cate.list');
                    echo"'
               </script>";
          }
          
          
     }
}
