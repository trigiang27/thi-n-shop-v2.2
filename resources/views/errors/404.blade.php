@extends('layouts.index')
@section('title','Giới thiệu - Hoa hồng Thái Lan - ThienYShop')
@section('content')
<div class="main-content col-md-9 col-md-push-3 textalign-center">
    <div class="title-error">Không tìm thấy trang!</div>
    <p>Xin lỗi bạn, chúng tôi không thể tìm kiếm được trang web bạn yêu cầu hoặc có gì đó đã sai... Bạn vui lòng kiểm tra lại hoặc trở lại <a href="/">Trang chủ</a>!</p>
</div>
@endsection