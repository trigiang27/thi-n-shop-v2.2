<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" >
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="content-language" content="vi" />
        <title>@yield('title')</title>
        <meta name="description" content="Hoa hồng leo Đà Nẵng, Đà Nẵng Rose" />
        <meta name="keywords" content="hoa hồng leo đà nẵng, hoa hồng đà nẵng, hoa hồng, huỳnh bá ý" />
        <meta name="robots" content="noodp,index,follow" />
        <meta name='revisit-after' content='2 days' />
        <!-- SEO Facebook -->
        @yield('meta-facebook')
        <base href="{!! asset('') !!}" />
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Reset CSS -->
        <link rel="stylesheet" href="css/normalize.css">
        <!-- My CSS -->
        <link rel="stylesheet" href="css/front/my.css">
        <!-- Slick -->
        <link rel="stylesheet" type="text/css" href="vendor/slick-1.6.0/slick/slick.css"/>
        <!-- Add the slick-theme.css if you want default styling -->
        <link rel="stylesheet" type="text/css" href="vendor/slick-1.6.0/slick/slick-theme.css"/>

        <link rel="shortcut icon" href="images/lo-go-valaw.ico" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
        <!-- Header -->
        @include('layouts.header')
        <!-- End header -->
        <h1 style="display: none">Hoa Hồng Đà Nẵng</h1>
        <!-- Banner slider -->
        @include('layouts.slide')
        <!-- End banner slider -->
        <div class="container">
            <div class="row">
                <!-- Main content -->
                @yield('content')
                <!-- End main-content -->
                <!-- Extra content -->
                @include('layouts.extra')
                <!-- End extra content -->
            </div><!-- End row -->
        </div><!-- End container -->
        <!-- Footer -->
        @include('layouts.footer')
        <!-- End footer -->
        <!-- jQuery -->
        <script type="text/javascript" src="js/jquery.js"></script>
        <!-- Bootstrap JavaScript -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <!-- Slick -->
        <script type="text/javascript" src="vendor/slick-1.6.0/slick/slick.min.js"></script>
        <!-- Elevate zoom -->
        <script type="text/javascript" src='js/jquery.elevatezoom.js'></script>
        <!-- my.js -->
        <script type="text/javascript" src="js/my.js"></script>
    </body>
</html>