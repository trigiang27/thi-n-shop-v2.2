<div class="banner-slider">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php
                $i = 0;
                foreach($banners as $banner){
                    echo '<li data-target="#myCarousel" data-slide-to="'.$i.'"></li>';
                    $i++;
                }
            ?>
        </ol>
        <div class="carousel-inner" role="listbox">
            @foreach($banners as $banner)
                <img class="item" src="/upload/banners/{!! $banner->name !!}" alt="{!! $banner->title !!}">
            @endforeach
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>