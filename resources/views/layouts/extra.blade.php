<div class="extra-content col-md-3 col-md-pull-9">
    <div class="heading">
        <h2 class="title">
            Danh mục sản phẩm
        </h2>
    </div>
    <ul>
        @foreach($cates as $item)
        <li><a href="san-pham/{!! $item->alias !!}">{!! $item->name !!}</a></li>
        @endforeach
    </ul>
    <div class="heading">
        <h2 class="title">
            <a href="san-pham-mua-nhieu">Sản phẩm mua nhiều</a> 
        </h2>
    </div>
    <div class="row">
        <div class="extra-buy-more-product">
            @foreach($salling_product as $product)
            <div class="product-1 col-xs-12">
                <div class="product-box">
                    <div class="row">
                        <div class="product-thumbnail col-xs-4 col-md-6">
                            <a href="chi-tiet/{!! $product->alias !!}" title="Sandal Converse cao cấp">
                                <img src="upload/{!! $product->image !!}" alt="Sandal Converse cao cấp">
                            </a>
                            @if($product->saleoff != 0)
                            <div class="sale-flash">12%</div>
                            @endif
                        </div>
                        <div class="col-xs-8 col-sm-6">
                            <h3 class="product-name">
                                <a href="chi-tiet/{!! $product->alias !!}" title="Sandal Converse cao cấp">{!! $product->name !!}</a>
                            </h3>
                            <div class="product-price">{!! number_format($product->price*(100-$product->saleoff)/100) !!}đ</div>
                            @if($product->saleoff != 0)
                            <div class="product-price-old">{!! number_format($product->price) !!}đ</div>
                            @else
                                <div class="product-price-old" style="color: white;">{!! number_format($product->price) !!}đ</div>
                            @endif
                            <p><a href="chi-tiet/{!! $product->alias !!}" class="btn btn-default">MUA HÀNG</a></p>
                        </div>
                    </div>
                </div>
            </div><!-- End product-1 -->
            @endforeach
        </div><!-- End extra-buy-more-product -->
    </div>
    <div class="heading">
        <h2 class="title">
            <a href="san-pham-moi">Sản phẩm mới</a> 
        </h2>
    </div>
    <div class="row">
        <div class="extra-new-product">
            @foreach($new_product as $product)
            <div class="product-1 col-xs-12">
                <div class="product-box">
                    <div class="row">
                        <div class="product-thumbnail col-xs-4 col-md-6">
                            <a href="chi-tiet/{!! $product->alias !!}" title="Sandal Converse cao cấp">
                                <img src="upload/{!! $product->image !!}" alt="Sandal Converse cao cấp">
                            </a>
                            @if($product->saleoff != 0)
                            <div class="sale-flash">12%</div>
                            @endif
                        </div>
                        <div class="col-xs-8 col-sm-6">
                            <h3 class="product-name">
                                <a href="chi-tiet/{!! $product->alias !!}" title="Sandal Converse cao cấp">{!! $product->name !!}</a>
                            </h3>
                            <div class="product-price">{!! number_format($product->price*(100-$product->saleoff)/100) !!}đ</div>
                            @if($product->saleoff != 0)
                            <div class="product-price-old">{!! number_format($product->price) !!}đ</div>
                            @endif
                            <p><a href="chi-tiet/{!! $product->alias !!}" class="btn btn-default">MUA HÀNG</a></p>
                        </div>
                    </div>
                </div>
            </div><!-- End product-1 -->
            @endforeach
        </div><!-- End extra-buy-more-product -->
    </div>
</div>