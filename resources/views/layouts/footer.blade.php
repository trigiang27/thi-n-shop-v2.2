<div id="footer">
    <div class="container">
        <div class="row">
            <div class="info-contact col-xs-12 col-md-6">
                <div class="row">
                    <div class="logo col-xs-12">
                        <h2><a href="">Hoa Hồng Đà Nẵng</a></h2>
                    </div><!-- End logo -->
                    <ul class="col-xs-12">
                        <li>
                            <i class="glyphicon glyphicon-map-marker"></i>
                            Số 4/2, đường số 3, Quận Ngũ Hành Sơn, Đà Nẵng
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-earphone"></i>
                             0937 932 766 - (0511) 3696969
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-envelope"></i>
                            hoahongdanang@gmail.com
                        </li>
                    </ul>
                </div>
            </div>
            <!-- End info-contact -->
            <div class="menu-footer col-xs-12 col-md-3">
                <div class="row">
                    <div class="logo col-xs-12">
                        <h2><a href="javascript:void(0)">Sản phẩm</a></h2>
                    </div><!-- End logo -->
                    <ul class="col-xs-12">
                        @foreach($cates as $item)
                        <li><a href="san-pham/{!! $item->alias !!}">{!! $item->name !!}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="intro-contact col-xs-12 col-md-3">
                <div class="row">
                    <div class="logo col-xs-12">
                        <h2><a href="javascript:void(0)">Về chúng tôi</a></h2>
                    </div><!-- End logo -->
                    <ul class="col-xs-12">
                        <li>
                            <a href="gioi-thieu">Giới thiệu</a>
                        </li>
                        <li>
                            <a href="lien-he">Liên hệ</a>
                        </li>
                        <li>
                            <a href=""><img class="fanpage" src="images/facebook-logo.jpg" alt="Fanpage"></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="bottom-footer col-xs-12">
                <div class="info-website">© Bản quyền thuộc về GiangNT</div>
            </div>
        </div><!-- End row -->
    </div><!-- End container -->
</div>