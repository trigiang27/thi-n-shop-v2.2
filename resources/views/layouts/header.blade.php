<div id="header">
    <div class="container top-header">
        <div class="logo">
            <a href=""><h1>HOA HỒNG ĐÀ NẴNG</h1></a>
        </div><!-- End logo -->
        <ul class="content-header">
            <li class="phone">
                <div class="align-items">
                    <i class="glyphicon glyphicon-earphone"></i>
                    <span>0937 932 766</span>
                </div>
            </li><!--
            --><li class="search">
                <div class="align-items">
                    <form action="tim-kiem" method="GET" role="search">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <button class="btn btn-search" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                        <input type="text" value="" name="tukhoa" placeholder="Tìm kiếm">
                    </form>
                </div>
            </li><!--
            --><li class="login">
                <div class="align-items">
                    <i class="glyphicon glyphicon-user"></i>
                    <a href="#">Đăng nhập</a>
                </div>
            </li><!--
            --><li class="register">
                <div class="aign-items">
                    <i class="glyphicon glyphicon-ok-sign"></i>
                    <a href="#">Đăng ký</a>
                </div>
            </li><!--
        --><li class="cart">
                <div class="align-items">
                    <a href="giohang">
                        <i class="glyphicon glyphicon-shopping-cart"></i>
                        <span>{!! $count_cart !!}</span>
                    Giỏ hàng</a>
                <div class="align-items">
            </li>
        </ul><!-- End content-header -->
    </div><!-- End container -->
    <!-- Wrap menu -->
    @include('layouts.menu')
    <!-- End wrap-menu -->
</div>