<div class="wrap-menu">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="button-right">
                    <div class="cart">
                        <a href="giohang">
                            <i class="glyphicon glyphicon-shopping-cart"></i>
                            <span class="cartCount">{!! $count_cart !!}</span>
                        </a>
                    </div>
                    <div class="search">
                        <form action="tim-kiem" method="GET" role="search">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <input type="text" value="" name="tukhoa" placeholder="Tìm kiếm">
                            <button class="btn-2" type="submit">
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                            <button class="btn-1" type="button">
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li class="trang-chu"><a href="{!! url('/') !!}">Trang chủ</a></li>
                    <li class="gioi-thieu"><a href="gioi-thieu">Giới thiệu</a></li>
                    <li class="dropdown san-pham">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sản phẩm <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            @foreach($cates as $item)
                            <li><a href="san-pham/{!! $item->alias !!}">{!! $item->name !!}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    <li class="thong-tin"><a href="thong-tin">Thông tin</a></li>
                    <li class="lien-he"><a href="lien-he">Liên hệ</a></li>
                    <li class="cham-soc"><a href="cham-soc">Cách chăm sóc</a></li>
                </ul>
            </div>
        </div><!-- End container -->
    </nav><!-- End navbar -->
</div>