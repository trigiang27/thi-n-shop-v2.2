@extends('admin.master')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Sản phẩm
                    <small>Sửa</small>
                </h1>
            </div>
            @include('admin.blocks.errors')
            @include('admin.blocks.thongbao')
            <!-- /.col-lg-12 -->
            <form action="" method="POST" enctype="multipart/form-data" name="frmEdit">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                <div class="col-lg-7" style="padding-bottom:120px">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <div class="form-group">
                        <label>Thể loại</label>
                        <select class="form-control" name="sltParent">
                            <option value="">Vui lòng chọn thể loại</option>
                            <?php cate_parent($cate,0,"",$data->cate_id); ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tên</label>
                        <input class="form-control" name="txtName" placeholder="Vui lòng nhập tên sản phẩm" value="{!! old('txtName',$data->name) !!}" />
                    </div>
                    <div class="form-group">
                        <label>Giá</label>
                        <input class="form-control" name="txtPrice" placeholder="Vui lòng nhập giá" value="{!! old('txtPrice',$data->price) !!}"/>
                    </div>
                    <div class="form-group">
                        <label>Khuyến mãi</label>
                        <input class="form-control" name="txtSaleOff" placeholder="Vui lòng nhập tỷ lệ khuyến mãi" value="{!! old('txtSaleOff',$data->saleoff) !!}"/>
                    </div>
                    <div class="form-group">
                        <label>Mô tả</label>
                        <textarea class="form-control" rows="3" name="txtDescription">{!! old('txtDescription',isset($data->description) ? $data->description : null) !!}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Nội dung</label>
                        <textarea class="form-control" rows="3" name="txtContent">{!! old('txtContent',isset($data->content) ? $data->content : null) !!}</textarea>
                        <script type="text/javascript">ckeditor("txtContent")</script>
                    </div>
                    <div class="form-group">
                        <label>Mới: </label>
                        <label class="radio-inline">
                            <input name="rdoNew" value="1" type="radio"
                                @if($data->new === 1)
                                checked=""
                                @endif
                            >Có
                        </label>
                        <label class="radio-inline">
                            <input name="rdoNew" value="0" type="radio"
                                @if($data->new === 0)
                                checked=""
                                @endif
                            >Không
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Nổi bật: </label>
                        <label class="radio-inline">
                            <input name="rdoHighLile" value="1" type="radio"
                                @if($data->highlight === 1)
                                checked=""
                                @endif
                            >Có
                        </label>
                        <label class="radio-inline">
                            <input name="rdoHighLile" value="0" type="radio"
                                @if($data->highlight === 0)
                                checked=""
                                @endif
                            >Không
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Bán chạy: </label>
                        <label class="radio-inline">
                            <input name="rdoSalling" value="1" type="radio"
                                @if($data->salling === 1)
                                checked=""
                                @endif
                            >Có
                        </label>
                        <label class="radio-inline">
                            <input name="rdoSalling" value="0" type="radio"
                                @if($data->salling === 0)
                                checked=""
                                @endif
                            >Không
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Tình trạng: </label>
                        <label class="radio-inline">
                            <input name="rdoStatus" value="1" type="radio"
                                @if($data->status === 1)
                                checked=""
                                @endif
                            >Còn hàng
                        </label>
                        <label class="radio-inline">
                            <input name="rdoStatus" value="0" type="radio"
                                @if($data->status === 0)
                                checked=""
                                @endif
                            >Hết hàng
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Ảnh gốc</label><br>
                        <img src="upload/{!! $data->image !!}" alt="" width="50%">
                        <input type="file" name="fImages">
                    </div>
                    <button type="submit" class="btn btn-success">Sửa</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                </div>
                <!-- <div class='col-md-1'></div>
                <div class='col-md-4 box-right'>
                    @foreach($data->product_images as $item)
                    <div class="image_detail" id="{!! $item->id !!}">
                        <lable>Hình ảnh chi tiết sản phẩm</lable>
                        <div class="product">
                        <img src="upload/detail/{!! $item->name !!}" alt="" width="100%" >
                        <a href="javascript:void(0)" type="button" id="close" class="btn btn-danger btn-circle"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    @endforeach
                    <button type="button" class="btn btn-primary" id="addImages">Thêm hình ảnh</button>
                    <div id="insert"></div>
                </div> -->
            <form>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection()
