@extends('admin.master')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Sản phẩm
                    <small>Danh sách</small>
                </h1>
            </div>
            @include('admin.blocks.thongbao')
            <!-- /.col-lg-12 -->
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr align="center">
                        <th>Stt</th>
                        <th>Tên</th>
                        <th>Giá</th>
                        <th>Khuyến mãi</th>
                        <th>Ảnh</th>
                        <th>Thể loại</th>
                        <th>Mới</th>
                        <th>Nổi bật</th>
                        <th>Bán chạy</th>
                        <th>Tình trạng</th>
                        <th>Xóa</th>
                        <th>Sửa</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0 ?>
                    @foreach($data as $item)
                    <tr class="odd gradeX" align="center">
                        <td>{!! ++$i !!}</td>
                        <td>{!! $item->name !!}</td>
                        <td>{!! number_format($item->price,0,",",".") !!} VNĐ</td>
                        <td>{!! $item->saleoff !!}%</td>
                        <td>
                            <img width="100px" src="upload/{!! $item->image !!}"/>
                        </td>
                        <td>
                            <?php 
                                $cate = DB::table('cates')->where('id',$item->cate_id)->first();
                                echo $cate->name;
                            ?>
                        </td>
                        <td>
                            <p>
                            @if($item->new == 1)
                                Có
                            @else
                                Không
                            @endif
                            </p>
                            <i id="{!! $item->id !!}" class="new glyphicon glyphicon-refresh"></i>
                        </td>
                        <td>
                            <p>
                            @if($item->highlight == 1)
                                Có
                            @else
                                Không
                            @endif
                            </p>
                            <i id="{!! $item->id !!}" class="highlight glyphicon glyphicon-refresh"></i>
                        </td>
                        <td>
                            <p>
                            @if($item->salling == 1)
                                Có 
                            @else
                                Không
                            @endif
                            </p>
                            <i id="{!! $item->id !!}" class="salling glyphicon glyphicon-refresh"></i>
                        </td>
                        <td>
                            <p>
                            @if($item->status == 1)
                                Còn hàng
                            @else
                                Hết hàng
                            @endif
                            </p>
                            <i id="{!! $item->id !!}" class="status glyphicon glyphicon-refresh"></i>
                        </td>
                        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/product/delete/{!! $item->id !!}" onclick="return xacnhanxoa('Bạn có thật sự muốn xóa')"> Xóa</a></td>
                        <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/product/edit/{!! $item->id !!}"> Sửa</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection()