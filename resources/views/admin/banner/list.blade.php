@extends('admin.master')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Banner
                </h1>
            </div>
            @include('admin.blocks.errors')
            @include('admin.blocks.thongbao')
            <!-- /.col-lg-12 -->
            <form action="admin/banner/create" name="frmEdit" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                <div class="col-lg-7" style="padding-bottom:120px">
                    <div>
                        <label>Banners hiện có:</label><br>
                        <?php $id = 1 ?>
                        @foreach($data as $item)
                        <div class="image_detail" id="{!! $item->id !!}">
                            <div class="banner">
                                <img src="upload/banners/{!! $item->name !!}" alt="" width="100%" >
                                <a href="javascript:void(0)" type="button" id="close" class="btn btn-danger btn-circle"><i class="fa fa-times"></i></a>
                            </div>
                            <div class="title">
                                <b>Title {!! $id++ !!}:</b>
                                <input class="form-control" type="text" name="title_{!! $item->id !!}" value="{!! $item->title !!}">
                                <i class="update_title glyphicon glyphicon-refresh" title="Cập nhật"></i>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class='col-md-1'></div>
                <div class='col-md-4 box-right'>
                    <div class="form-group add_image">
                        <h2>Thêm hình ảnh: </h2>
                        <lable>Hình ảnh chi tiết sản phẩm</lable>
                        <input type="file" name="fBanner"/>
                        <b>Title: </b>
                        <input type="text" required="" name="title_banner" value="{!! old('title_banner') !!}"><br>
                        <button type="submit" class="btn btn-success btn-them">Thêm</button>
                    </div>
                </div>
            <form>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection()
