@extends('admin.master')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Chăm sóc
                    <small>Sửa</small>
                </h1>
            </div>
            @include('admin.blocks.errors')
            @include('admin.blocks.thongbao')
            <!-- /.col-lg-12 -->
            <form action="admin/noi-dung/cham-soc" method="POST" enctype="multipart/form-data" name="frmEdit">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                <div class="col-lg-7" style="padding-bottom:120px">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <div class="form-group">
                        <label>Nội dung</label>
                        <textarea class="form-control" rows="5" name="txtContent">{!! $content[0]->chamsoc !!}</textarea>
                        <script type="text/javascript">ckeditor("txtContent")</script>
                    </div>
                    <button type="submit" class="btn btn-success">Sửa</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                </div>
            <form>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection()
