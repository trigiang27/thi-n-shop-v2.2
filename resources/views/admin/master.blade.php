<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Khóa Học Lập Trình Laravel Framework 5.x Tại Khoa Phạm">
    <meta name="author" content="Vu Quoc Tuan">
    <title>Admin - Hoa Hồng Đà Nẵng</title>
    <base href="{{ asset('') }}">
    <!-- Bootstrap Core CSS -->
    <link href="admin1/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="admin1/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="admin1/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="admin1/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <link href="admin1/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="admin1/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <link rel="shortcut icon" href="images/lo-go-valaw.ico" />
    
    <script>
        var baseURL = "{!! asset('') !!}";
    </script>

    <!-- My CSS -->
    <link href="admin1/css/my.css" rel="stylesheet">

    <!-- Ckeditor && Ckfinder -->
    <script src="admin1/js/ckeditor/ckeditor.js"></script>

    <!-- Ckfinder -->
    <script src="admin1/js/ckfinder/ckfinder.js"></script>

    <script src="admin1/js/func_ckfinder.js"></script>
    <!-- End Ckeditor && Ckfinder -->
</head>

<body>
    
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="admin/product/list">Admin - Hoa Hồng Đà Nẵng</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> {!! Auth::user()->username !!}</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Cài đặt</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="admin/logout"><i class="fa fa-sign-out fa-fw"></i> Đăng xuất</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a><i class="fa fa-dashboard fa-fw"></i> Bảng điều khiển</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Thể loại<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/cate/list">Danh sách</a>
                                </li>
                                <li>
                                    <a href="admin/cate/create">Tạo</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-cube fa-fw"></i> Sản phẩm<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/product/list">Danh sách</a>
                                </li>
                                <li>
                                    <a href="admin/product/create">Tạo</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> Thành viên<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/user/list">Danh sách</a>
                                </li>
                                <li>
                                    <a href="admin/user/create">Tạo</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> Quản lý nội dung<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/banner/list">Banner</a>
                                </li>
                                <li>
                                    <a href="admin/noi-dung/gioi-thieu">Giới thiệu</a>
                                </li>
                                <li>
                                    <a href="admin/noi-dung/thong-tin">Thông tin</a>
                                </li>
                                <li>
                                    <a href="admin/noi-dung/cham-soc">Chăm sóc</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        @yield('content')
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="admin1/bower_components/jquery/dist/jquery.min.js"></script>
    
    <script type="text/javascript">
        $( document ).ready(function() {
            $(".new ,.highlight, .salling, .status").click(function (event){
                var id = $(this).attr("id");
                var property = $(this).attr("class").split(" ")[0];
                $.ajax({
                    url: 'admin/product/update-property/'+id+'/'+property,
                    type: 'GET',
                    cache: false,
                    data: {"id":id,"property":property},
                    success:function(data){
                        if(property == "status"){
                            if(data == 0){
                                $(event.target).siblings('p')[0].innerText = "Hết hàng";
                            } else {
                                $(event.target).siblings('p')[0].innerText = "Còn hàng";
                            }
                        } else {
                            if(data == 0){
                                $(event.target).siblings('p')[0].innerText = "Không";
                            } else {
                                $(event.target).siblings('p')[0].innerText = "Có";
                            }
                        }
                    }
                });
            });
        });
    </script>

    <!-- Bootstrap Core JavaScript -->
    <script src="admin1/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="admin1/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="admin1/dist/js/sb-admin-2.js"></script>

    <!-- DataTables JavaScript -->
    <script src="admin1/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>

    <script src="admin1/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <!-- MyScript -->
    <script src="admin1/js/myscript.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    
</body>

</html>
