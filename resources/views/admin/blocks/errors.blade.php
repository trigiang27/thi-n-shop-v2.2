<!-- Thong bao loi tu validate -->
<div class="col-lg-12">
@if (count($errors)>0)
    <dir class="alert alert-danger hide-after-3s">
        <ul>
            @foreach($errors->all() as $error)
                <li>{!! $error !!}</li>
            @endforeach
        </ul>
    </dir>
@endif
</div>

