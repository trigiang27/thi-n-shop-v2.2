@extends('admin.master')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thể loại
                    <small>Sửa</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                @include('admin.blocks.errors')
                @include('admin.blocks.thongbao')
                <form action="" method="POST">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <div class="form-group" style="display: none;">
                        <label>Thể loại cha</label>
                        <select class="form-control" name="sltParent">
                            <option value="0">Vui lòng chọn thể loại</option>
                            <?php cate_parent($parent,0,"",$data->parent_id); ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tên loại sản phẩm</label>
                        <input class="form-control" name="txtCateName" placeholder="Vui lòng nhập tên thể loại" value="{!! old('txtCateName',$data->name) !!}"/>
                    </div>
                    <button type="submit" class="btn btn-success">Sửa</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                <form>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection()