@extends('admin.master')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thể loại
                    <small>Tạo</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                @include('admin.blocks.errors')
                @include('admin.blocks.thongbao')
                <form action="admin/cate/create" method="POST">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <div class="form-group" style="display: none;">
                        <label>Thể loại cha</label>
                        <select class="form-control" name="sltParent">
                            <option value="">Vui lòng chọn thể loại</option>
                            <?php cate_parent($parent); ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tên thể loại</label>
                        <input class="form-control" name="txtCateName" placeholder="Vui lòng nhập tên thể loại" />
                    </div>
                    <button type="submit" class="btn btn-success">Tạo</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection()