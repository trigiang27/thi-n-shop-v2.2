@extends('admin.master')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thể loại
                    <small>Danh sách</small>
                </h1>
            </div>
            @include('admin.blocks.thongbao')
            <!-- /.col-lg-12 -->
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr align="center">
                        <th>Stt</th>
                        <th>Tên</th>
                        <!-- <th>Thể loại cha</th> -->
                        <th>Xóa</th>
                        <th>Sửa</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $i = 0;
                    ?>
                    @foreach($data as $item)
                    <tr class="odd gradeX" align="center">
                        <td>{!! ++$i !!}</td>
                        <td>{!! $item->name !!}</td>
                        <!-- <td>
                            @if($item->parent_id == "0")
                                None
                            @else
                                <?php
                                    $parent = DB::table('cates')->where('id',$item->parent_id)->first();
                                    echo $parent->name;
                                ?>
                            @endif
                        </td> -->
                        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/cate/delete/{!! $item->id !!}" onclick="return xacnhanxoa('Bạn có thật sự muốn xóa')"> Xóa</a></td>
                        <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/cate/edit/{!! $item->id !!}">Sửa</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection()