@extends('admin.master')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thành viên
                    <small>Tại</small>
                </h1>
            </div>
            @include('admin.blocks.errors')
            @include('admin.blocks.thongbao')
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                <form action="" method="POST">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <div class="form-group">
                        <label>Tên tài khoản</label>
                        <input class="form-control" name="txtUser" placeholder="Vui lòng nhập tên tài khoản" value="{!! old('txtUser') !!}" />
                    </div>
                    <div class="form-group">
                        <label>Mật khẩu</label>
                        <input type="password" class="form-control" name="txtPass" placeholder="Vui lòng nhập mật khẩu" />
                    </div>
                    <div class="form-group">
                        <label>Xác nhận mật khẩu</label>
                        <input type="password" class="form-control" name="txtRePass" placeholder="Vui lòng xác nhận mật khẩu" />
                    </div>
                    <div class="form-group">
                        <label>Cấp độ tài khoản</label>
                        <label class="radio-inline">
                            <input name="rdoLevel" value="1" checked="" type="radio">Admin
                        </label>
                        <label class="radio-inline">
                            <input name="rdoLevel" value="2" type="radio">Member
                        </label>
                    </div>
                    <button type="submit" class="btn btn-success">Tạo</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection()