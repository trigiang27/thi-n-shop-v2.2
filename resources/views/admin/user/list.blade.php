@extends('admin.master')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tài khoản
                    <small>Danh sách</small>
                </h1>
            </div>
            @include('admin.blocks.errors')
            @include('admin.blocks.thongbao')
            <!-- /.col-lg-12 -->
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr align="center">
                        <th>Stt</th>
                        <th>Tên tài khoản</th>
                        <th>Cấp độ</th>
                        <th>Xóa</th>
                        <th>Sửa</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=0 ?>
                    @foreach($data as $user)
                    <tr class="odd gradeX" align="center">
                        <td>{!! ++$i !!}</td>
                        <td>{!! $user->username !!}</td>
                        <td>
                            @if($user->id == 1)
                                SuperAdmin
                            @elseif($user->level == 1)
                                Admin
                            @else
                                Member
                            @endif
                        </td>
                        <td class="center"><i class="fa fa-trash-o fa-fw"></i><a href="admin/user/delete/{!! $user->id !!}"> Xóa</a></td>
                        <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/user/edit/{!! $user->id !!}"> Sửa</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection()