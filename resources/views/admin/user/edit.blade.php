@extends('admin.master')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thành viên
                    <small>Sửa</small>
                </h1>
            </div>
            @include('admin.blocks.errors')
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                <form action="" method="POST">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <div class="form-group">
                        <label>Tên tài khoản</label>
                        <input class="form-control" name="txtUser" value="{!! $data->username !!}" disabled />
                    </div>
                    <div class="form-group">
                        <label>Mật khẩu</label>
                        <input type="password" class="form-control" name="txtPass" placeholder="Vui lòng nhập mật khẩu" />
                    </div>
                    <div class="form-group">
                        <label>Xác nhận mật khẩu</label>
                        <input type="password" class="form-control" name="txtRePass" placeholder="Vui lòng xác nhận mật khẩu" />
                    </div>
                    <!-- <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="txtEmail" placeholder="Please Enter Email" />
                    </div> -->
                    @if(Auth::user()->id == 1 && $data->id != 1)
                    <div class="form-group">
                        <label>Cấp độ</label>
                        <label class="radio-inline">
                            <input name="rdoLevel" value="1" type="radio"
                                @if($data->level == 1)
                                    checked=""
                                @endif
                            >Admin
                        </label>
                        <label class="radio-inline">
                            <input name="rdoLevel" value="2" type="radio"
                                @if($data->level == 2)
                                    checked=""
                                @endif
                            >Member
                        </label>
                    </div>
                    @endif
                    <button type="submit" class="btn btn-success">Sửa</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection()