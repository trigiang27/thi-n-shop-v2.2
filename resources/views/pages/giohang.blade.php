@extends('layouts.index')
@section('title','Giỏ hàng - Hoa hồng Thái Lan - Hoa Hồng Đà Nẵng')
@section('content')
<div class="main-content col-md-9 col-md-push-3">
    <div class="header">
        <h2 class="title-page-cart">Giỏ hàng</h2>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <table class="table table-hover table-cart">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <thead>
                        <tr>
                            <th>HÌNH ẢNH</th>
                            <th>TÊN SẢN PHẨM</th>
                            <th>ĐƠN GIÁ</th>
                            <th>SỐ LƯỢNG</th>
                            <th>THÀNH TIỀN</th>
                            <th>TÌNH TRẠNG</th>
                            <th>XÓA</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $product)
                        <tr>
                            <td>
                                <a class="img-product-cart" href="chi-tiet/{!! $product->options->alias !!}"  title="{!! $product->name !!}">
                                    <img src="upload/{!! $product->options->img !!}" alt="{!! $product->name !!}">
                                </a>
                            </td>
                            <td>
                                <h2>
                                    <a class="name-product-cart" href="chi-tiet/{!! $product->options->alias !!}">{!! $product->name !!}</a>
                                </h2>
                            </td>
                            <td>
                                <span class="product-price">{!! number_format($product->price) !!}</span>
                            </td>
                            <td>
                                <input type="number" title="Số lượng" class="inp-number" value="{!! $product->qty !!}" name="qty_{!! $product->rowId !!}" min="1" max="12">
                            </td>
                            <td>
                                <span class="product-price price_qty">{!! number_format($product->price*$product->qty) !!}</span>
                            </td>
                            <td>
                                <?php
                                    if($product->options->status){
                                        echo '<span class="" title="Tình trạng">Còn hàng</span>';
                                    } else {
                                        echo '<span class="" style="color:red;" title="Tình trạng">Hết hàng</span>';
                                    }
                                ?>
                            </td>
                            <td>
                                <span class="{!! $product->rowId !!} button glyphicon glyphicon-trash" title="Xóa"></span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-xs-12 btn-cart">
                    <button class="btn btn-lg btn-plus" title="Tiếp tục mua hàng" type="button" onclick="window.location.href=''">TIẾP TỤC MUA HÀNG</button>
                    <button class="btn btn-lg update-cart" title="Cập nhật giỏ hàng">CẬP NHẬP GIỎ HÀNG</button>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td class="text-xs-left">Tổng tiền thanh toán</td>
                                <td class="text-xs-right"><div class="product-price total">{!! $total !!}₫</div></td>
                            </tr>
                        </tbody>
                    </table>
                    <button class="btn btn-lg" title="Tiến hành thanh toán" type="button" onclick="
                            $flag = false;
                            $('.table-cart').find('span').each(function (){
                                if($(this).html() == 'Hết hàng'){
                                    $flag = true;
                                }
                            })
                            if($flag){
                                if(confirm('Có một số sản phẩm hết hàng bạn có thật sự muốn tiếp tục? \nNhững sản phẩm hết hàng chúng tôi sẽ thông báo khi có hàng. \nBấm \'OK\' để tiếp tục.')){
                                window.location.href='dat-hang';
                                }
                            } else {
                                window.location.href='dat-hang';
                            }
                    "><span>TIẾN HÀNH ĐẶT HÀNG</span></button>
                </div>
            </div>
        </div>
    </div><!-- End row -->
</div>
@endsection