@extends('layouts.index')
@section('title')
{!! $product->name !!} - Hoa hồng Thái Lan - Hoa Hồng Đà Nẵng
@endsection
@section('meta-facebook')
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{!! $product->name !!} - Hoa hồng Thái Lan - Chuyên cung cấp hoa hồng leo - Hoa Hồng Đà Nẵng" />
    <meta property="og:description" content="{!! $product->description !!}" />
    <meta property="og:image" content="{!! asset('') !!}upload/{!! $product->image !!}" />
    <meta property="og:image:width" content="400" />
    <meta property="og:image:height" content="300" />
@endsection
@section('content')
<div class="main-content col-md-9 col-md-push-3">
    <div class="row">
        <div class="product-details">
            <div class="images col-md-5">
                <img id="detail-image" src="upload/{!! $product->image !!}" alt="{!! $product->name !!}">
            </div>
            <div class="info col-md-7">
                <h1 class="product-name">{!! $product->name !!}</h1>
                <ul class="info-pro">
                    <li>
                        <b>Thể loại: </b>
                        {!! $product->cate->name !!}
                    </li>
                    <li>
                        <b>Tình trạng: </b>
                        @if($product->status == 1)
                        Còn hàng
                        @else
                        Hết hàng
                        @endif
                    </li>
                </ul>
                <div class="price">
                <div class="product-price">{!! number_format($product->price*(100-$product->saleoff)/100) !!}đ</div>
                @if($product->saleoff != 0)
                <div class="product-price-old">{!! number_format($product->price) !!}đ</div>
                @endif
                </div>
                <form enctype="multipart/form-data" id="add-to-cart-form" action="giohang/add/{!! $product->id !!}" method="post" class="form-inline">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <label class="">Số lượng: </label>
                    <input type="number" class="input-text qty" title="Qty" value="1" min="1" max="12" id="qty" name="quantity">
                    <button class="btn btn-lg btn-style btn-style-active btn-cart add_to_cart" title="Thêm vào giỏ hàng"><span><i class="icon-basket"></i>THÊM VÀO GIỎ HÀNG</span></button>
                    <label class="label-form-pro call-phone"><span class="hidden-xs-down">&nbsp;&nbsp;&nbsp;</span>Gọi <a href="tel: " title="Mua hàng">(04) 6674 2332</a> để được trợ giúp</label>
                </form>
            </div>
            <div class="detail-product col-xs-12">
                <label class="">THÔNG TIN SẢN PHẨM</label>
                <p>{!! $product->content !!}</p>
            </div>
        </div>
    </div>
    <div class="heading">
        <h2 class="title">
            <a href="{!! $product->alias !!}/san-pham-lien-quan">Sản phẩm liên quan</a> 
        </h2>
    </div>
    <div class="row">
        <div class="new-product">
            @foreach($concern_product as $product)
            <div class="product col-xs-6 col-sm-4">
                <div class="product-box">
                    <div class="product-thumbnail">
                        <a href="chi-tiet/{!! $product->alias !!}" title="{!! $product->name !!}">
                            <img src="upload/{!! $product->image !!}" alt="{!! $product->name !!}">
                        </a>
                    </div>
                    <h3 class="product-name">
                        <a href="chi-tiet/{!! $product->alias !!}" title="{!! $product->name !!}">Sandal cao cấp</a>
                    </h3>
                    @if($product->saleoff != 0)
                    <div class="sale-flash">{!! $product->saleoff !!}%</div>
                    @endif
                    <div class="product-price">{!! number_format($product->price*(100-$product->saleoff)/100) !!}đ</div>
                    @if($product->saleoff != 0)
                    <div class="product-price-old">{!! number_format($product->price) !!}đ</div>
                    @endif
                    <p><a href="chi-tiet/{!! $product->alias !!}" class="btn btn-default">MUA HÀNG</a></p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection