@extends('layouts.index')
@section('title','Chăm sóc hoa - Cách chăm sóc hoa hồng hiệu quả')
@section('content')
<div class="tutorial-page main-content col-md-9 col-md-push-3">
    <div class="heading">
        <h2 class="title textalign-left">Cách chăm sóc hoa</h2>
    </div>
    <div class="row">
        <div class="col-xs-12 textalign-left">
            {!! $data->chamsoc !!}
        </div>
    </div><!-- End row -->
</div>
@endsection