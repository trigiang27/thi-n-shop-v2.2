@extends('layouts.index')
@section('title','Hoa hồng Thái Lan - Chuyên cung cấp hoa hồng leo - Hoa Hồng Đà Nẵng')
@section('meta-facebook')
    <meta property="og:type" content="website" />
  <meta property="og:title" content="Hoa hồng Thái Lan - Chuyên cung cấp hoa hồng leo - Hoa Hồng Đà Nẵng" />
  <meta property="og:description" content="Hoa hồng leo Đà Nẵng, nơi cung cấp hoa uy tín nhất. hoa hồng được nhập khẩu trực tiếp từ Thái Lan..." />
  <meta property="og:image" content="{!! asset('') !!}/upload/TBAd_novalis.jpg" />
@endsection
@section('content')
<div class="main-content col-md-9 col-md-push-3">
    <div class="heading">
        <h2 class="title">
            <a href="san-pham-moi">Sản phẩm mới</a> 
        </h2>
    </div>
    <div class="row">
        <div class="new-product">
            @foreach($new_product as $product)
            <div class="product col-xs-6 col-sm-4">
                <div class="product-box">
                    <div class="product-thumbnail">
                        <a href="chi-tiet/{!! $product->alias !!}" title="Sandal Converse cao cấp">
                            <img src="upload/{!! $product->image !!}" alt="{!! $product->name !!}">
                        </a>
                    </div>
                    <h3 class="product-name">
                        <a href="chi-tiet/{!! $product->alias !!}" title="Sandal Converse cao cấp">{!! $product->name !!}</a>
                    </h3>
                    @if($product->saleoff != 0)
                    <div class="sale-flash">{!! $product->saleoff !!}%</div>
                    @endif
                    <div class="product-price">{!! number_format($product->price*(100-$product->saleoff)/100) !!}đ</div>
                    @if($product->saleoff != 0)
                    <div class="product-price-old">{!! number_format($product->price) !!}đ</div>
                    @endif
                    <p><a href="chi-tiet/{!! $product->alias !!}" class="btn btn-default">MUA HÀNG</a></p>
                </div>
            </div><!-- End product -->
            @endforeach
        </div><!-- End new-product -->
    </div><!-- End row -->
    <div class="heading">
        <h2 class="title">
            <a href="san-pham-khuyen-mai">Sản phẩm khuyến mãi</a> 
        </h2>
    </div>
    <div class="row">
        <div class="sale-off-product">
            @foreach($saleoff_product as $product)
            <div class="product col-xs-6 col-sm-4">
                <div class="product-box">
                    <div class="product-thumbnail">
                        <a href="chi-tiet/{!! $product->alias !!}" title="Sandal Converse cao cấp">
                            <img src="upload/{!! $product->image !!}" alt="Sandal Converse cao cấp">
                        </a>
                    </div>
                    <h3 class="product-name">
                        <a href="chi-tiet/{!! $product->alias !!}" title="Sandal Converse cao cấp">{!! $product->name !!}</a>
                    </h3>
                    @if($product->saleoff != 0)
                    <div class="sale-flash">{!! $product->saleoff !!}%</div>
                    @endif
                    <div class="product-price">{!! number_format($product->price*(100-$product->saleoff)/100) !!}đ</div>
                    @if($product->saleoff != 0)
                    <div class="product-price-old">{!! number_format($product->price) !!}đ</div>
                    @endif
                    <p><a href="chi-tiet/{!! $product->alias !!}" class="btn btn-default">MUA HÀNG</a></p>
                </div>
            </div><!-- End product -->
            @endforeach
        </div><!-- End sale-off-product -->
    </div><!-- End row -->
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <div class="heading">
                <h2 class="title">
                    <a href="san-pham-noi-bat">Sản phẩm nổi bật</a> 
                </h2>
            </div>
            <div class="row">
                <div class="highlight-product">
                    @foreach($highlight_product as $product)
                    <div class="product-2 col-xs-12">
                        <div class="product-box">
                            <div class="product-thumbnail">
                                <a href="chi-tiet/{!! $product->alias !!}" title="Sandal Converse cao cấp">
                                    <img src="upload/{!! $product->image !!}" alt="{!! $product->name !!}">
                                </a>
                            </div>
                            <h3 class="product-name">
                                <a href="chi-tiet/{!! $product->alias !!}" title="Sandal Converse cao cấp">{!! $product->name !!}</a>
                            </h3>
                            @if($product->saleoff != 0)
                            <div class="sale-flash">{!! $product->saleoff !!}%</div>
                            @endif
                            <div class="product-price">{!! number_format($product->price*(100-$product->saleoff)/100) !!}đ</div>
                            @if($product->saleoff != 0)
                            <div class="product-price-old">{!! number_format($product->price) !!}đ</div>
                            @endif
                            <p><a href="chi-tiet/{!! $product->alias !!}" class="btn btn-default">MUA HÀNG</a></p>
                        </div>
                    </div><!-- End product -->
                    @endforeach
                </div><!-- End highlight-product -->
            </div>
        </div>
        <div class="col-xs-12 col-md-8">
            <div class="heading">
                <h2 class="title">
                    <a href="san-pham-mua-nhieu">Sản phẩm mua nhiều</a> 
                </h2>
            </div>
            <div class="row">
                <div class="buy-more-product">
                    @foreach($salling_product as $product)
                    <div class="product-1 col-xs-12 col-sm-6">
                        <div class="product-box">
                            <div class="row">
                                <div class="product-thumbnail col-xs-4 col-md-6">
                                    <a href="chi-tiet/{!! $product->alias !!}" title="Sandal Converse cao cấp">
                                        <img src="upload/{!! $product->image !!}" alt="Sandal Converse cao cấp">
                                    </a>
                                    @if($product->saleoff != 0)
                                    <div class="sale-flash">12%</div>
                                    @endif 
                                </div>
                                <div class="col-xs-8 col-sm-6">
                                    <h3 class="product-name">
                                        <a href="chi-tiet/{!! $product->alias !!}" title="Sandal Converse cao cấp">{!! $product->name !!}</a>
                                    </h3>
                                    <div class="product-price">{!! number_format($product->price*(100-$product->saleoff)/100) !!}đ</div>
                                    @if($product->saleoff != 0)
                                    <div class="product-price-old">{!! number_format($product->price) !!}đ</div>
                                    @else
                                    <div class="product-price-old" style="color: white;">{!! number_format($product->price) !!}đ</div>
                                    @endif
                                    <p><a href="chi-tiet/{!! $product->alias !!}" class="btn btn-default">MUA HÀNG</a></p>
                                </div>
                            </div>
                        </div>
                    </div><!-- End product-1 -->
                    @endforeach
                </div><!-- End buy-more-product -->
            </div>
        </div>
    </div><!-- row -->
</div>
@endsection