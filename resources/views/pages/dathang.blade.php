@extends('layouts.index')
@section('title','Đặt hàng - Hoa hồng Thái Lan - Hoa Hồng Đà Nẵng')
@section('content')
<div class="main-content col-md-9 col-md-push-3">
    <div class="header">
        <h2 class="title-page-cart">Thông tin đặt hàng</h2>
    </div>
    <div class="row">
        <div class="dathang col-xs-12">
            <form action="dat-hang" method="post">
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="info textalign-left">
                    @include('admin.blocks.errors')
                    @include('admin.blocks.thongbao')
                    <div class="form-group">
                        <label for="">Họ và tên: </label>
                        <input class="form-control" name="txtName" value="{!! old('txtName') !!}" placeholder="Vui lòng nhập họ và tên"/>
                    </div>
                    <div class="form-group">
                        <label>Số điện thoại: </label>
                        <input class="form-control" name="txtPhone" value="{!! old('txtPhone') !!}" placeholder="Vui lòng nhập số điện thoại"/>
                    </div>
                    <div class="form-group">
                        <label>Địa chỉ: </label>
                        <input class="form-control" name="txtAddress" value="{!! old('txtAddress') !!}" placeholder="Vui lòng nhập địa chỉ nhận hàng"/>
                    </div>
                    <div class="form-group">
                        <label>Mô tả: </label>
                        <textarea class="form-control" rows="3" name="txtNote" value="{!! old('txtNote') !!}" placeholder="Ghi chú"></textarea>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover table-cart">
                        <thead>
                            <tr>
                                <th>HÌNH ẢNH</th>
                                <th>TÊN SẢN PHẨM</th>
                                <th>ĐƠN GIÁ</th>
                                <th>SỐ LƯỢNG</th>
                                <th>THÀNH TIỀN</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $product)
                            <tr>
                                <td>
                                    <a class="img-product-cart" href="chi-tiet/{!! $product->options->alias !!}"  title="{!! $product->name !!}">
                                        <img src="upload/{!! $product->options->img !!}" alt="{!! $product->name !!}">
                                    </a>
                                </td>
                                <td>
                                    <h2>
                                        <a class="name-product-cart" href="chi-tiet/{!! $product->options->alias !!}">{!! $product->name !!}</a>
                                    </h2>
                                </td>
                                <td><span class="product-price">{!! number_format($product->price) !!}</span></td>
                                <td>
                                    <input type="number" title="Số lượng" class="inp-number" value="{!! $product->qty !!}" name="qty_{!! $product->rowId !!}" min="1" max="12" readonly="">
                                </td>
                                <td><span class="product-price">{!! number_format($product->price*$product->qty) !!}</span></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-xs-12 btn-cart">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td class="text-xs-left">Tổng tiền thanh toán</td>
                                    <td class="text-xs-right"><div class="product-price">{!! $total !!}₫</div></td>
                                </tr>
                            </tbody>
                        </table>
                        <label class="note">Chú ý: Tiền vận chuyển sẽ được thông báo với khách khi nhân viên xác nhận đơn hàng (Từ 0 đến 50.000đ tùy vào nơi nhận hàng).</label>
                        <button class="btn btn-lg" title="Gửi mail đặt hàng" type="submit"><span>ĐẶT HÀNG</span></button>
                    </div>
                </div>
            </form>
        </div>
    </div><!-- End row -->
</div>
@endsection