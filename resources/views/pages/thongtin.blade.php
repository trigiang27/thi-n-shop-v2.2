@extends('layouts.index')
@section('title','Thông tin - Hoa hồng Thái Lan - Hoa Hồng Đà Nẵng')
@section('content')
<div class="main-content col-md-9 col-md-push-3">
    <div class="heading">
        <h2 class="title textalign-left">Thông tin</h2>
    </div>
    <div class="row">
        <div class="col-xs-12 textalign-left">
            {!! $data->thongtin !!}
        </div>
        <iframe class="col-lg-10" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3835.1166117970406!2d108.25430594993698!3d16.00744398886477!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTbCsDAwJzI2LjgiTiAxMDjCsDE1JzIzLjQiRQ!5e0!3m2!1svi!2s!4v1478955259967" width="100%" height="300px" frameborder="0" style="border:0;" allowfullscreen></iframe>
    </div><!-- End row -->
</div>
@endsection