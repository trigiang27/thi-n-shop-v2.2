@extends('layouts.index')
@section('title','Liên hệ - Hoa hồng Thái Lan - Hoa Hồng Đà Nẵng')
@section('content')
<div class="main-content col-md-9 col-md-push-3">
    <div class="row">
        <div class="contact col-xs-12">
            <div class="header">
                <h2 class="title-page-contact">Liên hệ với chúng tôi</h2>
                @include('admin.blocks.errors')
                <p>Bạn hãy điền nội dung tin nhắn vào form dưới đây và gửi cho chúng tôi. Chúng tôi sẽ trả lời bạn sau khi nhận được.</p>
            </div>
            <div class="form">
                <form accept-charset='UTF-8' action='lien-he' id='contact' method='post'>
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <div class="form-signup clearfix">
                        <fieldset class="form-group">
                            <label>Tên:</label>
                            <input type="text" name="name" class="form-control  form-control-lg" placeholder="Vui lòng nhập tên của bạn" value="{!! old('name') !!}">
                        </fieldset>
                        <fieldset class="form-group">
                            <label>Số điện thoại:</label>
                            <input type="text" name="sdt" class="form-control form-control-lg" placeholder="Vui lòng nhập số điện thoại" value="{!! old('sdt') !!}">
                        </fieldset>
                        <fieldset class="form-group">
                            <label>Nội dung:</label>
                            <textarea name="message" class="form-control form-control-lg" rows="5" placeholder="Vui lòng nhập nội dung tin nhắn">{!! old('message') !!}</textarea>
                        </fieldset>
                        <div class="pull-xs-left" style="margin-top:20px;">
                            <button tyle="summit" class="btn btn-success">GỬI TIN NHẮN</button>
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div><!-- End row -->
</div>
@endsection