@extends('layouts.index')
@section('title')
    {!! $cate->name !!} - Hoa hồng Thái Lan - Hoa Hồng Đà Nẵng
@endsection
@section('content')
<div class="main-content col-md-9 col-md-push-3">
    <div class="heading">
        <h2 class="title">{!! $cate->name !!}</h2>
        <p>Tổng cộng có {!! $products->total() !!} sản phẩm</p>
    </div>
    <div class="row">
        <div class="cate">
        @foreach($products as $product)
        <div class="product col-xs-6 col-sm-4">
            <div class="product-box">
                <div class="product-thumbnail">
                    <a href="chi-tiet/{!! $product->alias !!}" title="Sandal Converse cao cấp">
                        <img src="upload/{!! $product->image !!}" alt="Sandal Converse cao cấp">
                    </a>
                </div>
                <h3 class="product-name">
                    <a href="chi-tiet/{!! $product->alias !!}" title="Sandal Converse cao cấp">{!! $product->name !!}</a>
                </h3>
                @if($product->saleoff != 0)
                <div class="sale-flash">{!! $product->saleoff !!}%</div>
                @endif
                <div class="product-price">{!! number_format($product->price*(100-$product->saleoff)/100) !!}đ</div>
                @if($product->saleoff != 0)
                <div class="product-price-old">{!! number_format($product->price) !!}đ</div>
                @endif
                <p><a href="chi-tiet/{!! $product->alias !!}" class="btn btn-default">MUA HÀNG</a></p>
            </div>
        </div><!-- End product -->
        @endforeach
        </div>
        <!-- Pagination -->
        <div class="col-xs-12 text-center">{{ $products->links() }}</div>
        <!-- End Pagination -->
    </div><!-- End row -->
</div>
@endsection