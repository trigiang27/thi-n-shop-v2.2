@extends('layouts.index')
@section('title','Giới thiệu - Hoa hồng Thái Lan - Hoa Hồng Đà Nẵng')
@section('content')
<div class="main-content col-md-9 col-md-push-3">
    <div class="heading">
        <h2 class="title textalign-left">Giới thiệu</h2>
    </div>
    <div class="row">
        <div class="col-xs-12 textalign-left">
            {!! $data->gioithieu !!}
        </div>
    </div><!-- End row -->
</div>
@endsection